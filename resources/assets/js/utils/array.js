export function array2object(arr) {
    return arr.reduce((res, item) => {
        return Object.assign({}, res, {
            [item.id]: item
        })
    }, {});
}

export function hasProperty(prop, val, arr) {
    return arr.some(item => item[prop] === val);
}

export function isEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}