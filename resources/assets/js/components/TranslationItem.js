import React, { Component } from 'react';
import classNames from 'classnames';
import { ListItem, ListItemSecondaryAction } from 'material-ui/List';
import Icon from 'material-ui/Icon';
import Menu, { MenuItem } from 'material-ui/Menu';

export default class TranslationItem extends Component {
    constructor() {
        super();
        this.state = {
            anchorEl: null,
            showMenu: false,
        };
    }

    render() {
        const { isDragging, connectDragSource, translation } = this.props;
        const { anchorEl, showMenu } = this.state;

        return connectDragSource(
            <div className={classNames('translation-item', {
                    'translation-item_dragging': isDragging,
                })}
                 onMouseEnter={() => this.showMenuIcon()}
                 onMouseLeave={() => this.hideMenuIcon()}
            >
                <ListItem divider>{this.props.translation.text}</ListItem>
                {
                    showMenu &&
                        <ListItemSecondaryAction>
                            <Icon
                                aria-label="More"
                                aria-owns={anchorEl ? 'more vert' : null}
                                aria-haspopup="true"
                                onClick={(e) => this.clickMore(e)}
                            >
                                <Icon>more_vert</Icon>
                            </Icon>
                            <Menu
                                id="long-menu"
                                anchorEl={anchorEl}
                                open={Boolean(anchorEl)}
                                onClose={(e) => this.closeMenu(e)}>
                                <MenuItem>
                                    <Icon onClick={() => this.props.delete(this.props.translation.id)}>delete</Icon>
                                </MenuItem>
                            </Menu>
                        </ListItemSecondaryAction>
                }

            </div>
        );
    }

    clickMore(event) {
        this.setState({ anchorEl: event.currentTarget });
    }

    closeMenu(event) {
        this.setState({ anchorEl: null });
    }

    showMenuIcon() {
        this.setState({ showMenu: true })
    }

    hideMenuIcon() {
        this.setState({ showMenu: false })
    }
}