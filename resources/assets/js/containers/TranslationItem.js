import TranslationItem from '../components/TranslationItem'
import { ItemTypes} from '../Constants';
import { connect } from 'react-redux';
import { DragSource } from 'react-dnd';

/**
 * Implements the drag source contract.
 */
const translationSource = {
    beginDrag(props) {
        return props.translation;
    }
};

/**
 * Specifies the props to inject into your component.
 */
function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    };
}

function mapStateToProps(state) {
    return {

    };
}

function mapDispatchToProps(dispatch) {
    return {

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DragSource(ItemTypes.TRANSLATION, translationSource, collect)(TranslationItem));