import axios from 'axios';
import { object2array } from '../utils/object'
import { hasProperty } from '../utils/array';
import * as ACTION_TYPES from './types';
import { validateRegister } from '../helpers/validator';
import { push } from 'react-router-redux';

export function changeSearchField(value) {
    return {
        type: ACTION_TYPES.CHANGE_SEARCH_FIELD,
        payload: {
            value
        }
    };
}

export function submitSearchField() {
    return {
        type: ACTION_TYPES.SUBMIT_CHANGE_SEARCH_FIELD,
    }
}

export function setWordDefinitionsData(data) {
    return {
        type: ACTION_TYPES.SET_WORD_DEFINITIONS_DATA,
        payload: {
            data
        }
    }
}

export function fetchDefinitions() {
    return (dispatch, getState) => {
        dispatch(submitSearchField());

        const state = getState();
        const ws = state.wordConstructor;

        if (!ws.word.length ||
            hasProperty('word', ws.word, object2array(ws.allDefinitions)) ||
            hasProperty('word', ws.word, object2array(ws.allExamples)) ||
            hasProperty('word', ws.word, object2array(ws.allTranslations))
            ) {
            return;
        }

        dispatch(setDefinitionLoadingStarted());

        axios.get('/api/constructor', {
            params: {
                word: getState().wordConstructor.word
            }
        })
        .then((response) => response.data)
        .then(response => {
            dispatch(setWordDefinitionsData(response))
        })
        .then(() => dispatch(setDefinitionLoadingFinished()));
    };


}

export function changeWordTranslation(value) {
    return {
        type: ACTION_TYPES.CHANGE_WORD_TRANSLATION,
        payload: {
            value
        }
    }
}

export function addWordTranslation(word, translation) {
    return {
        type: ACTION_TYPES.ADD_WORD_TRANSLATION,
        payload: {
            word,
            translation,
        }
    }
}

export function deleteWordTranslation(id) {
    return {
        type: ACTION_TYPES.DELETE_WORD_TRANSLATION,
        payload: {
            id,
        }
    }
}

export function moveTranslationToDefinition(translationId, definitionId) {
    return {
        type: ACTION_TYPES.MOVE_TRANSLATION_TO_DEFINITION,
        payload: {
            translationId,
            definitionId,
        }
    }
}

export function removeTranslationFromDefinition(translationId) {
    return {
        type: ACTION_TYPES.REMOVE_TRANSLATION_FROM_DEFINITION,
        payload: {
            translationId
        }
    }
}

export function setDefinitionLoadingStarted() {
    return {
        type: ACTION_TYPES.DEFINITION_LOADING_STARTED
    }
}

export function setDefinitionLoadingFinished() {
    return {
        type: ACTION_TYPES.DEFINITION_LOADING_FINISHED
    }
}

export function deleteExample(id) {
    return {
        type: ACTION_TYPES.DELETE_EXAMPLE,
        payload: {
            id,
        }
    }
}

export function moveExampleToDefinition(exampleId, definitionId) {
    return {
        type: ACTION_TYPES.MOVE_EXAMPLE_TO_DEFINITION,
        payload: {
            exampleId,
            definitionId,
        }
    }
}

export function moveExampleToOthers(exampleId) {
    return {
        type: ACTION_TYPES.MOVE_EXAMPLE_TO_OTHERS,
        payload: {
            exampleId,
        }
    }
}

export function register(name, email, password, passwordConfirm) {
    return dispatch => {
        const errors = validateRegister(name, email, password, passwordConfirm);

        if (Object.keys(errors).length) {
            dispatch(registerRequestFailure(errors));
            return;
        }

        dispatch(registerSendRequest());

        axios.post('/api/oauth/register', {
            name,
            email,
            password,
            password_confirmation: passwordConfirm
        })
            .then((response) => {
                dispatch(recieveToken(email, password));
                dispatch(registerRequestSuccess());
            })
            .catch(error => {
                let response = error.response;

                if (response.status === 422) { // Form validation error
                    let errors = {};

                    for (let prop in response.data.errors) {
                        errors[prop] = response.data.errors[prop][0];
                    }

                    dispatch(registerRequestFailure(errors));
                }
            })
    };
}

export function registerSendRequest() {
    return {
        type: ACTION_TYPES.REGISTER_SEND_REQUEST,
    }
}

export function registerRequestSuccess() {
    return {
        type: ACTION_TYPES.REGISTER_REQUEST_SUCCESS
    }
}

export function registerRequestFailure(errors) {
    return {
        type: ACTION_TYPES.REGISTER_REQUEST_FAILURE,
        payload: {
            errors,
        }
    }
}

export function removeFormError(field) {
    return {
        type: ACTION_TYPES.REMOVE_FORM_ERROR,
        payload: {
            field,
        }
    }
}

export function recieveToken(email, password) {
    return dispatch => {
        dispatch(tokenSendRequest());

        axios.post('/oauth/token', {
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            grant_type: 'password',
            username: email,
            password: password,
            scope: '*',
        })
            .then(response => response.data)
            .then(response => dispatch(tokenRequestSuccess(response)))
            .then(()=> dispatch(push('/constructor')))
            .catch(response => dispatch(tokenRequestFailure()))
    }
}

export function tokenSendRequest() {
    return {
        type: ACTION_TYPES.TOKEN_SEND_REQUEST,
    }
}

export function tokenRequestSuccess(data) {
    return {
        type: ACTION_TYPES.TOKEN_REQUEST_SUCCESS,
        payload: {
            data
        }
    }
}

export function tokenRequestFailure() {
    return {
        type: ACTION_TYPES.TOKEN_REQUEST_FAILURE
    }
}

export function logout() {
    return dispatch => {
        dispatch(removeToken());

        dispatch(push('/login'));
    }

}

export function removeToken() {
    return {
        type: ACTION_TYPES.REMOVE_TOKEN,
    }
}

export function loginFlushWrongCredentials() {
    return {
        type: ACTION_TYPES.LOGIN_FLUSH_WRONG_CREDENTIALS
    }
}

export function setRedirectUrl(url) {
    return {
        type: ACTION_TYPES.SET_REDIRECT_URL,
        payload: {
            url,
        }
    }
}

export function submitWordConstructor(word, definitions) {
    return dispatch => {
        dispatch(submitNewWordRequest());

        axios.post('/api/constructor', {
            word,
            definitions,
        })
            .then(response => response.data)
            .then(response => dispatch(submitNewWordSuccess()))
            .catch(response => dispatch(submitNewWordFailure()))
    }
}

export function submitNewWordRequest() {
    return {
        type: ACTION_TYPES.SUBMIT_NEW_WORD_REQUEST,
    };
}

export function submitNewWordSuccess() {
    return {
        type: ACTION_TYPES.SUBMIT_NEW_WORD_SUCCESS,
    };
}

export function submitNewWordFailure() {
    return {
        type: ACTION_TYPES.SUBMIT_NEW_WORD_FAILURE,
    };
}

export function loadTraining(params = {}) {
    return dispatch => {
        dispatch(trainingRequest());

        axios.get('/api/training', {params,})
            .then(response => response.data)
            .then(response => dispatch(trainingSuccess(response.words)))
            .catch(error => {
                console.error(error.stack)
                dispatch(trainingFailure())
            })
    }
}

export function trainingRequest() {
    return {
        type: ACTION_TYPES.TRAINING_REQUEST,
    }
}

export function trainingFailure() {
    return {
        type: ACTION_TYPES.TRAINING_FAILURE,
    }
}

export function trainingSuccess(words) {
    return {
        type: ACTION_TYPES.TRAINING_SUCCESS,
        payload: {
            words,
        }
    }
}

export function setTrainingCount(count) {
    return {
        type: ACTION_TYPES.SET_TRAINING_COUNT,
        payload: {
            count,
        }
    }
}

export function loadDictionary(params = {}, options = {}) {
    return dispatch => {
        dispatch(dictionaryRequest());

        axios.get('/api/dictionary', {
            params,
        })
            .then(response => dispatch(dictionarySuccess(response.data, options)))
            .catch(response => dispatch(dictionaryFailure()))
    }
}

function dictionaryRequest() {
    return {
        type: ACTION_TYPES.DICTIONARY_REQUEST,
    }
}

function dictionarySuccess(words, options) {
    return {
        type: ACTION_TYPES.DICTIONARY_SUCCESS,
        payload: {
            words,
            options,
        }
    }
}

function dictionaryFailure() {
    return {
        type: ACTION_TYPES.DICTIONARY_FAILURE,
    }
}