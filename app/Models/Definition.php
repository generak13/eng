<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Definition extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        'text',
        'provider',
    ];

    protected $casts = [
        'text' => 'string',
        'provider' => 'string',
    ];

    public function word(): BelongsTo {
        return $this->belongsTo(Word::class);
    }

    /**
     * The word definitions
     * @return hasMany
     */
    public function sentences(): hasMany
    {
        return $this->hasMany(Sentence::class);
    }

    public function wordTranslations(): hasMany
    {
        return $this->hasMany(Translation::class);
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                return false;
            }
        });
    }

    public function setId(string $id): Definition {
        $this->id = $id;
        return $this;
    }

    public function setText(string $text): Definition {
        $this->text = $text;
        return $this;
    }

    public function setProvider(string $provider): Definition {
        $this->provider = $provider;
        return $this;
    }
}