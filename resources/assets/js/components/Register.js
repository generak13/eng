import React, { Component } from 'react';
import { isEqual } from "../utils/array";
import { Link } from "react-router";
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { LinearProgress } from 'material-ui/Progress';
import { FormControl, FormHelperText } from 'material-ui/Form';

export default class Register extends Component {
    constructor() {
        super();

        this.state = {
            name: '',
            email: '',
            password: '',
            password_confirmation: '',
            errorFields: [],
        };
    }


    componentWillReceiveProps(nextProps) {
        let errorFields = Object.keys(nextProps.errors);

        if (!isEqual(errorFields, this.state.errorFields)) {
            this.setState({
                errorFields,
            });
        }
    }

    render() {
        let errors = this.props.errors;

        return (
            <Paper elevation={4} className={'register-wrapper'}>
                <Typography variant="headline" component="h3">
                    Enter your account information
                </Typography>

                <FormControl disabled={this.props.loading}>
                    <div>
                        <TextField
                            label="Name"
                            value={this.state.name}
                            margin='normal'
                            inputRef={(input) => this.nameInput = input}
                            error={'name' in errors}
                            helperText={errors.name}
                            onChange={(e) => this.onChange(e, 'name')}
                            onKeyPress={(e) => this.catchReturn(e)}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Email"
                            value={this.state.email}
                            margin='normal'
                            inputRef={(input) => this.emailInput = input}
                            error={'email' in errors}
                            helperText={errors.email}
                            onChange={(e) => this.onChange(e, 'email')}
                            onKeyPress={(e) => this.catchReturn(e)}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Password"
                            value={this.state.password}
                            type="password"
                            autoComplete="current-password"
                            margin="normal"
                            inputRef={(input) => this.passwordInput = input}
                            error={'password' in errors}
                            helperText={errors.password}
                            onChange={(e) => this.onChange(e, 'password')}
                            onKeyPress={(e) => this.catchReturn(e)}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Password Confirmation"
                            value={this.state.password_confirmation}
                            type="password"
                            autoComplete="current-password"
                            margin="normal"
                            inputRef={(input) => this.passwordConfirmationInput = input}
                            error={'password_confirmation' in errors}
                            helperText={errors.password_confirmation}
                            onChange={(e) => this.onChange(e, 'password_confirmation')}
                            onKeyPress={(e) => this.catchReturn(e)}
                        />
                    </div>
                    <Button variant="raised" color="primary" className={'register-submit'} onClick={() => this.processSubmit()}>Register</Button>

                    <div>
                        or you <Link to={'/login'}>Login</Link> if you have an account
                    </div>
                </FormControl>

                { this.props.loading && <LinearProgress /> }
            </Paper>
        );
    }

    onChange(e, field) {
        this.setState({
            [field]: e.target.value
        });

        if (~this.state.errorFields.indexOf(field)) {
            this.props.removeFormError(field);
        }
    }

    catchReturn(e) {
        if (e.key === 'Enter') {
            this.processSubmit();

            e.preventDefault();
        }
    }

    processSubmit() {
        const { name, email, password, password_confirmation } = this.state;

        this.props.submit(name, email, password, password_confirmation);
    }
}