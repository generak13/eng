<?php

namespace App\Factories;

use App\Models\Definition;
use App\Models\Word;

class DefinitionFactory {
    public static function findOrCreate(array $data, Word $word): Definition {
        ['id' => $id, 'text' => $text] = $data;

        $definition = Definition::query()->find($id);

        if (!($definition instanceof Definition)) {
            $definition = new Definition();
            $definition->setId($id);
            $definition->setText($text);
            $definition->setProvider('unknown');

            $definition->word()->associate($word);

            $definition->save();
        }

        return $definition;
    }
}