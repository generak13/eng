import TranslationsList from '../components/TranslationsList'
import { connect } from 'react-redux';
import { ItemTypes } from '../Constants';
import { DropTarget } from 'react-dnd';
import * as actions from '../actions'

/* React DnD */
const listTarget = {
    drop(props, monitor) {
        props.addDefinitionTranslation(monitor.getItem().id, props.definition.id);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
    };
}


/* Redux connect */
function mapStateToProps(state) {
    return { //receive props from parent
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addDefinitionTranslation: (translationId, definitionId) => {
            dispatch(actions.moveTranslationToDefinition(translationId, definitionId));
        },
        deleteTranslation: (translationId) => {
            dispatch(actions.deleteWordTranslation(translationId));
        }
    };
}

const droppableTarget = DropTarget(ItemTypes.TRANSLATION, listTarget, collect)(TranslationsList);
export default connect(mapStateToProps, mapDispatchToProps)(droppableTarget);