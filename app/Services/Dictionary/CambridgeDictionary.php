<?php

namespace App\Services\Dictionary;

use Symfony\Component\DomCrawler\Crawler;

class CambridgeDictionary
{
    private static $baseUrl = 'https://dictionary.cambridge.org/ru/%D1%81%D0%BB%D0%BE%D0%B2%D0%B0%D1%80%D1%8C/%D0%B0%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9';

    private $def2examples = [];

    public function loadData(string $word)
    {
        try {
            $crawler = new Crawler(file_get_contents(self::$baseUrl . '/' . $word));
        } catch (\Exception $e) {
            return $this->def2examples;
        }

        $crawler->filter('.def-block')->each(function ($definitionBlock) {
            $definitionElements = $definitionBlock->filter('.def');

            if ($definitionElements->count() == 0) {
                return;
            }

            $definition = $definitionElements->first()
                ->text();
            $definition = ucfirst(trim(trim($definition), ':;')); // trim spaces and characters like :; and capitalize first symbol

            $examples = $definitionBlock->filter('.examp')
                ->each(function ($exampleBlock) {
                    return trim($exampleBlock->text());
                });

            $this->def2examples[$definition] = $examples;
        });
    }

    public function getDefinitions2Examples()
    {
        return $this->def2examples;
    }
}