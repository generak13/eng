import React from 'react';
import { connect } from 'react-redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import WordConstructor from '../components/WordConstructor';
import * as actions from '../actions'
import {object2array} from '../utils/object';

function mapStateToProps(state) {
    const wc = state.wordConstructor;
    const word = wc.word;

    let definitions = object2array(wc.allDefinitions)
        .filter(definition => definition.word === word)
        .map(definition => {
            return Object.assign({}, definition, {
                translations: definition.translations.map(translationId => wc.allTranslations[translationId]),
                examples: definition.examples.map(exampleId => wc.allExamples[exampleId])
            });
        });

    const canSubmit = definitions.some(definition => {
        return definition.text && definition.translations.length && definition.examples.length;
    });

    return {
        isNew: state.wordConstructor.isNew,
        loading: state.wordConstructor.loading,
        word,
        definitions: definitions,
        canSubmit: canSubmit,
        addedWords: state.wordConstructor.addedWords,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        submitConstructor: (word, definitions) => {
            definitions = definitions.filter(definition => {
                return definition.text && definition.examples.length && definition.translations.length;
            });
            dispatch(actions.submitWordConstructor(word, definitions));
        }
    };
}

const dragDropContext = DragDropContext(HTML5Backend)(WordConstructor);
export default connect(mapStateToProps, mapDispatchToProps)(dragDropContext);