<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Translation extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        'text',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'text' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function definition(): BelongsTo {
        return $this->belongsTo(Definition::class);
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                return false;
            }
        });
    }

    public function setId(string $id): Translation {
        $this->id = $id;
        return $this;
    }

    public function setText(string $text): Translation {
        $this->text = $text;
        return $this;
    }
}
