import React, { Component } from 'react'
import classNames from 'classnames'
import Icon from 'material-ui/Icon'
import Typography from 'material-ui/Typography'
import ExpansionPanel, {
    ExpansionPanelSummary,
    ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel'
import List from 'material-ui/List'
import { ListItem, ListItemText } from 'material-ui/List'

export default class Definition extends Component {
    constructor() {
        super()

        this.state = {
            expanded: false,
        }
    }

    toggleExpanded() {
        this.setState(prevState => {
            return {
                expanded: !prevState.expanded,
            }
        })
    }

    render() {
        const { definition } = this.props
        const { expanded } = this.state

        return (
            <ExpansionPanel
                expanded={expanded}
                onChange={() => this.toggleExpanded()}>

                <ExpansionPanelSummary
                    style={{userSelect: 'auto'}}
                    expandIcon={<Icon>expand_more</Icon>}
                >
                    <Typography>{ definition.text }</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <List>
                        {
                            definition.sentences.map(sentence =>
                                <ListItem divider key={sentence.id}>
                                    <ListItemText
                                        key={sentence.id}
                                        primary={<Typography type="body" style={{userSelect: 'auto'}}>{sentence.text}</Typography>}
                                        secondary={sentence.translation || null}
                                    />
                                </ListItem>
                            )
                        }
                    </List>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        )
    }
}