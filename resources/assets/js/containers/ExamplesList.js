import { connect } from 'react-redux';
import { ItemTypes } from "../Constants";
import { DropTarget } from 'react-dnd';
import ExamplesList from '../components/ExamplesList'
import * as actions from '../actions'

/* React DnD */
const listTarget = {
    drop(props, monitor) {
        props.addDefinitionExample(monitor.getItem().id, props.definition.id);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
    };
}


/* Redux connect */
function mapStateToProps(state) {
    return { //receive props from parent
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addDefinitionExample: (exampleId, definitionId) => {
            dispatch(actions.moveExampleToDefinition(exampleId, definitionId));
        },
        deleteExample: (id) => {
            dispatch(actions.deleteExample((id)));
        }
    };
}

const droppableTarget = DropTarget(ItemTypes.EXAMPLE, listTarget, collect)(ExamplesList);
export default connect(mapStateToProps, mapDispatchToProps)(droppableTarget);