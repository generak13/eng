<?php

namespace App\Factories;

use App\Models\User;
use App\Models\Word;
use Webpatser\Uuid\Uuid;

class WordFactory {
    public static function findOrCreate(array $data, User $user): Word {
        ['text' => $text] = $data;

        $word = Word::query()
            ->where('text', $text)
            ->where('user_id', $user->id)
            ->first();

        if (!($word instanceof Word)) {
            $word = new Word();
            $word->setId(Uuid::generate(4));
            $word->setText($text);
            $word->user()->associate($user);
            $word->save();
        }

        return $word;
    }
}