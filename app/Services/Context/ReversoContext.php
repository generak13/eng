<?php

namespace App\Services\Context;

use Symfony\Component\DomCrawler\Crawler;

class ReversoContext
{
    private static $baseUrl = 'http://context.reverso.net/%D0%BF%D0%B5%D1%80%D0%B5%D0%B2%D0%BE%D0%B4/%D0%B0%D0%BD%D0%B3%D0%BB%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9-%D1%80%D1%83%D1%81%D1%81%D0%BA%D0%B8%D0%B9';

    private $sentences2data = [];

    public function loadData(string $word)
    {
        try {
            $crawler = new Crawler(file_get_contents(self::$baseUrl . '/' . $word));
        } catch (\Exception $e) {
            return $this->sentences2data;
        }

        $crawler->filter('#examples-content > .example')
            ->each(function (Crawler $node) {
                $source = trim($node->filter('.src > .text')->first()->text());
                $translation = trim($node->filter('.trg > .text')->first()->text());

                $translationHighlight = $node->filter('.trg > .text em')->each(function ($em) {
                    return trim($em->text());
                });

                $this->sentences2data[$source] = [
                    'translation' => $translation,
                    'highlights' => $translationHighlight,
                ];
            });
    }

    public function getSamples(): array
    {
        return $this->sentences2data;
    }
}