import React, { Component } from 'react';
import SearchWord from './SearchWord';
import Definition from '../containers/Definition';
import TranslationBlock from './TranslationBlock';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Snackbar from 'material-ui/Snackbar';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { LinearProgress } from 'material-ui/Progress';
import OtherExamplesList from '../containers/OtherExamplesList';

export default class WordConstructor extends Component {
    constructor() {
        super();

        this.state = {
            opened: false,
            snackWord: '',
        }
    }

    componentWillReceiveProps(nextProps) {
        const diffAddedWords = nextProps.addedWords.filter(item => this.props.addedWords.indexOf(item) < 0);

        if (diffAddedWords.length) {
            this.setState({
                opened: true,
                snackWord: diffAddedWords[0],
            });
        }
    }

    handleSnackbarClose() {
        this.setState({ opened: false });
    }

    render() {
        let content = null;

        if (this.props.loading) {
            content = <LinearProgress />;
        } else if (this.props.definitions.length) {
            content = (
                <div>
                    <Grid container>
                        <Grid item md={9}>
                            { this.props.definitions.map(definition => <Definition key={definition.id} definition={definition}/>) }
                        </Grid>
                        <Grid item md={3}>
                            <TranslationBlock/>
                        </Grid>
                    </Grid>
                    <Paper elevation={4}>
                        <OtherExamplesList />
                    </Paper>
                </div>
            );
        }

        const snackbar = (
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={this.state.opened}
                onClose={this.handleSnackbarClose.bind(this)}
                SnackbarContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span>Word "{this.state.snackWord}" added succesfully. Let's add another word!</span>}
            />
        );

        return (
            <Grid container spacing={0}>
                <div className={'constructor-wrapper'}>
                    <div className="content padding-small">
                        <SearchWord/>

                        {content}
                    </div>

                    <Button variant="fab" color="primary" aria-label="add" disabled={!this.props.canSubmit} className={'submit-constructor'} onClick={() => this.props.submitConstructor(this.props.word, this.props.definitions)}>
                        <Icon>{this.props.isNew ? 'add' : 'edit'}</Icon>
                    </Button>
                </div>
                {snackbar}
            </Grid>
        );
    }
}