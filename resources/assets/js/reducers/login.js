import * as ACTION_TYPES from '../actions/types';

const initState = {
    wrongCredentials: false,
    loading: false,
};

export default function (state = initState, action) {
    switch (action.type) {
        case ACTION_TYPES.TOKEN_SEND_REQUEST:
            state = Object.assign({}, state, {
                loading: true,
            });

            return state;
        case ACTION_TYPES.TOKEN_REQUEST_SUCCESS:
            state = Object.assign({}, state, {
                loading: false,
                wrongCredentials: false,
            });

            return state;
        case ACTION_TYPES.TOKEN_REQUEST_FAILURE:
            state = Object.assign({}, state, {
                loading: false,
                wrongCredentials: true,
            });

            return state;
        case ACTION_TYPES.LOGIN_FLUSH_WRONG_CREDENTIALS:
            state = Object.assign({}, state, {
                wrongCredentials: false,
            });

            return state;
        default:
            return state;
    }
}