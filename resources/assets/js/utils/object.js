export function object2array(obj) {
    return Object.keys(obj).map(id => obj[id]);
}