import { connect } from 'react-redux';
import { push } from 'react-router-redux'
import Layout from '../components/Layout'
import * as actions from '../actions';

/* Redux connect */
function mapStateToProps(state, ownProps) {
    return Object.assign({}, ownProps, {
        isLoggedIn: state.user.isLoggedIn,
    });
}

function mapDispatchToProps(dispatch) {
    return {
        selectMenuItem: item => {
            switch (item.id) {
                case 'logout':
                    dispatch(actions.logout());
                    break;
                default:
                    dispatch(push(item.pathname));
            }


        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
