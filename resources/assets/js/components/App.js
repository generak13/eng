import React from 'react';
import { Router, Route, Redirect } from 'react-router'
import { Provider } from 'react-redux'
import WordConstructor from '../containers/WordConstructor'
import Login from '../containers/Login'
import Register from '../containers/Register'
import AuthManager from '../containers/AuthManager';
import EnsureLoggedInContainer from '../containers/EnsureLoggedInContainer';
import EnsureLoggedOutContainer from '../containers/EnsureLoggedOutContainer';

import Layout from '../containers/Layout'
import Training from '../containers/Training';
import Dictionary from '../containers/Dictionary';
import Video from '../components/Video'

const App = ({ store, history }) => (
    <Provider store={store}>
        <Router history={history}>
            <Route path='/' component={Layout}>
                <Route path='/' component={AuthManager}>
                    <Route component={EnsureLoggedOutContainer}>
                        <Route exact path="/" component={WordConstructor} />
                        <Route path="register" component={Register} />
                        <Route path="login" component={Login} />
                    </Route>

                    <Route component={EnsureLoggedInContainer}>
                        <Route path="/training" component={Training} />
                        <Route path="/dictionary" component={Dictionary} />
                        <Route path="/video" component={Video} />
                        <Route path="app" component={WordConstructor} />
                        <Route path="constructor" component={WordConstructor} />
                    </Route>
                </Route>
            </Route>
            <Redirect from='*' to='/constructor' />
        </Router>
    </Provider>
);

export default App;