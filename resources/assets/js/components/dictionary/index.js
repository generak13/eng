import React, { Component } from 'react';
import { withStyles } from 'material-ui/styles'
import Table, {
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel
} from 'material-ui/Table'
import CircularProgress from 'material-ui/Progress/CircularProgress'
import TextField from 'material-ui/TextField'
import Definition from "./Definition"

const styles = theme => ({
    alignCenter: {
        textAlign: 'center',
    },
    moreLink: {
        textDecoration: 'none',
        '&:visited': {
            color: 'blue',
        }
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 300,
    }
})

class Dictionary extends Component {
    constructor() {
        super()

        this.state = {
            sortASC: false,
            searchWord: '',
            lastSearchedWord: '',
        }
    }

    componentDidMount() {
        this.props.loadDictionary()
    }

    getLoadDictionaryParams() {
        const { searchWord, sortASC } = this.state

        const params = {
            sortASC,
        }

        if (searchWord.length) {
            params.word = searchWord
        }

        return params
    }

    getLoadDictionaryOptions() {
        const { searchWord, lastSearchedWord } = this.state

        return {
            replace: searchWord !== lastSearchedWord
        }
    }

    searchWord(e) {
        e.preventDefault()

        const { searchWord, lastSearchedWord, sortASC } = this.state
        const params = this.getLoadDictionaryParams()
        const options = this.getLoadDictionaryOptions()

        this.setState({lastSearchedWord: searchWord})

        this.props.loadDictionary(params, options)
    }

    changeSearchField(e) {
        this.setState({searchWord: e.target.value});
    }

    loadMore(e) {
        e.preventDefault()

        const { words } = this.props
        const params = this.getLoadDictionaryParams()
        const options = this.getLoadDictionaryOptions()

        if (words.length) {
            params.from = words[words.length - 1].created_at
        }

        this.props.loadDictionary(params, options);
    }

    toggleSortDirection(e) {
        e.preventDefault()

        const params = this.getLoadDictionaryParams()
        const options = this.getLoadDictionaryOptions()

        params.sortASC = !params.sortASC
        this.setState({sortASC: params.sortASC})

        options.replace = true

        this.props.loadDictionary(params, options)
    }

    render() {
        const { classes, loading, words, hasMoreWords } = this.props
        const { sortASC } = this.state

        return (
            <div>
                <form noValidate autoComplete="off" className={classes.alignCenter} onSubmit={(e) => this.searchWord(e)}>
                    <TextField
                        id="word"
                        label="Search word"
                        className={classes.textField}
                        value={this.state.searchWord}
                        onChange={(e) => this.changeSearchField(e)}
                        margin="normal"
                    />
                </form>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <TableSortLabel
                                    active={true}
                                    direction={sortASC ? 'asc' : 'desc'}
                                    onClick={e => this.toggleSortDirection(e)}
                                >
                                    Word
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>Definitions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {words.map(word => {
                            return (
                                <TableRow key={word.id}>
                                    <TableCell component="th" scope="row">
                                            {word.text}
                                    </TableCell>
                                    <TableCell>
                                        {
                                            word.definitions.map(definition => {
                                                return (
                                                    <Definition
                                                        key={definition.id}
                                                        definition={definition}
                                                    />
                                                )
                                        })}
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
                {loading &&
                    <div className={classes.alignCenter}>
                        <CircularProgress />
                    </div>
                }
                {hasMoreWords && !loading &&
                    <div className={classes.alignCenter}>
                        <a
                            href='#'
                            className={classes.moreLink}
                            onClick={(e) => this.loadMore(e)}
                        >
                            more
                        </a>
                    </div>
                }
            </div>
        )
    }
}

export default withStyles(styles)(Dictionary)