import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import * as actions from '../actions';
import GlobalTranslationsList from '../containers/GlobalTranslationsList'
import { connect } from 'react-redux';
import {object2array} from '../utils/object';

class TranslationBlock extends Component {
    render() {
        return (
            <div className="translation-block">
                <div className="translation-block__list">
                    <GlobalTranslationsList />
                </div>

                <form onSubmit={(e) => this.props.addWordTranslation(e, this.props.word, this.props.translation)}>
                    <TextField
                        id="name"
                        label="Add new translation"
                        value={this.props.translation}
                        // className={classes.textField}
                        onChange={this.props.changeWordTranslation}
                        margin="normal"
                    />
                </form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const wc = state.wordConstructor;
    const word = wc.word;

    const definitionsTranslations = object2array(wc.allDefinitions)
        .filter(definition => definition.word === word)
        .map(definition => definition.translations)
        .reduce((arr, currents) => {
            return arr.concat(currents);
        }, []);

    return {
        word: wc.word,
        translation: wc.liveTranslation,
        translations: Object.keys(wc.allTranslations)
            .filter(tId => definitionsTranslations.indexOf(tId) === -1)
            .filter(tId => wc.allTranslations[tId].word === wc.word)
            .map(tId => wc.allTranslations[tId])
    };
}

function mapDispatchToProps(dispatch) {
    return {
        changeWordTranslation: (e) => {
            e.preventDefault();

            dispatch(actions.changeWordTranslation(e.target.value));
        },
        addWordTranslation: (e, word, translation) => {
            e.preventDefault();

            dispatch(actions.addWordTranslation(word, translation));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(TranslationBlock);