<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Migrations\Migration;

class GenerateClientsEncryptionKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('passport:keys', [
            '--force' => 1
        ]);

        Artisan::call('passport:client', [
            '--password' => true,
            '--name' => config('app.name').' Password Grant Client'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('TRUNCATE TABLE `oauth_clients`');
    }
}
