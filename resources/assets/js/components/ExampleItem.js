import React, { Component } from 'react';
import classNames from 'classnames';
import { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import Menu, { MenuItem } from 'material-ui/Menu';
import Fade from 'material-ui/transitions/Fade';

export default class ExampleItem extends Component {
    constructor() {
        super();
        this.state = {
            anchorEl: null,
            showMenu: false,
        };
    }

    render() {
        const { isDragging, connectDragSource, example, definitions, addDefinitionExample } = this.props;
        const { anchorEl, showMenu } = this.state;

        return connectDragSource(
            <div
                className={classNames('example-item', {'example-item_dragging': isDragging})}
                onMouseEnter={() => this.showMenuIcon()}
                onMouseLeave={() => this.hideMenuIcon()}
            >
                <ListItem divider secondary={'hello'}>
                    <ListItemText
                        primary={<Typography type="body" style={{userSelect: 'auto'}}>{example.text}</Typography>}
                        secondary={example.translation ? example.translation : null}
                    />
                </ListItem>
                {
                    showMenu &&
                        <ListItemSecondaryAction>
                            <Icon
                                aria-label="More"
                                aria-owns={anchorEl ? 'more_vert' : null}
                                aria-haspopup="true"
                                onClick={(e) => this.clickMore(e)}
                            >
                                <Icon>more_vert</Icon>
                            </Icon>
                            <Menu
                                id="fade-menu"
                                anchorEl={anchorEl}
                                open={Boolean(anchorEl)}
                                onClose={(e) => this.closeMenu(e)}
                                transition={Fade}>
                                {
                                    definitions.map(definition =>
                                        <MenuItem
                                            key={definition.id}
                                            onClick={() => addDefinitionExample(example.id, definition.id)}>
                                            { definition.text }
                                        </MenuItem>)
                                }
                                <MenuItem onClick={() => this.props.delete(example.id)}>Delete</MenuItem>
                            </Menu>
                        </ListItemSecondaryAction>
                }
            </div>
        );
    }

    clickMore(event) {
        this.setState({ anchorEl: event.currentTarget });
    }

    closeMenu(event) {
        this.setState({ anchorEl: null });
    }

    showMenuIcon() {
        this.setState({ showMenu: true })
    }

    hideMenuIcon() {
        this.setState({ showMenu: false })
    }
}