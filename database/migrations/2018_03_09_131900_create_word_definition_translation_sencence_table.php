<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordDefinitionTranslationSencenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_d_t_s', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('word_id');
            $table->string('definition_id')->nullable()->index();
            $table->string('translation_id')->nullable()->index();
            $table->string('sentence_id')->nullable()->index();
            $table->timestamps();
            $table->date('deleted_at')->index();

            $table->primary('id');

            $table->foreign('word_id')
                ->references('id')->on('words')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_d_t_s');
    }
}
