import React from 'react';
import { connect } from 'react-redux';
import Training from '../components/Training';
import * as actions from '../actions';
import { object2array } from '../utils/object'

function mapStateToProps(state) {
    const { loading, trainingCount, sentencesOrder, allSentences, allDefinitions, allTranslations } = state.training
    let ts = state.training;

    return {
        loading: loading,
        trainingCount: trainingCount,
        sentences: sentencesOrder.map(sentenceId => {
            let sentence = allSentences[sentenceId];
            let definition = object2array(allDefinitions).find(definition => ~definition.sentences.indexOf(sentenceId));

            return {
                ...sentence,
                definition: {
                    id: definition.id,
                    text: definition.text,
                },
                translations: definition.translations.map(translationId => {
                    return {
                        id: allTranslations[translationId].id,
                        text: allTranslations[translationId].text,
                    }
                })
            }
        }),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loadTraining: (params) => dispatch(actions.loadTraining(params)),
        setTrainingCount: (count) => dispatch(actions.setTrainingCount(count))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Training);