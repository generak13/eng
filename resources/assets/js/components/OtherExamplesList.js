import React, { Component } from 'react';
import classNames from 'classnames';
import List from 'material-ui/List';
import ExampleItem from '../containers/ExampleItem';

export default class OtherExamplesList extends Component {
    render() {
        if (!this.props.examples.length) {
            return null;
        }

        const { connectDropTarget, isOver, canDrop } = this.props;

        return connectDropTarget(
            <div className={classNames('examples-list', {
                'examples-list_drag-over': isOver,
                'examples-list_can-drop': !isOver && canDrop,
            })}>
                <List>
                    {this.props.examples.map(example => {
                        return <ExampleItem key={example.id} example={example} delete={this.props.deleteExample} />
                    })}
                </List>
            </div>
        );
    }
}