<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordTranslationCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_t_c', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('word_id');
            $table->string('translation_id');
            $table->string('category_id');

            $table->primary('id');

            $table->foreign('word_id')
                ->references('id')->on('words')
                ->onDelete('cascade');

            $table->foreign('translation_id')
                ->references('id')->on('translations')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_t_c');
    }
}
