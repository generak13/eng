import TranslationsList from '../components/TranslationsList'
import { connect } from 'react-redux';
import { ItemTypes } from '../Constants';
import { DropTarget } from 'react-dnd';
import * as actions from '../actions';
import {object2array} from '../utils/object';

/* React DnD */
const listTarget = {
    drop(props, monitor) {
        props.moveTranslationToGlobals(monitor.getItem().id);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
    };
}


/* Redux connect */
function mapStateToProps(state) {
    const wc = state.wordConstructor;
    const word = wc.word;

    const definitionsTranslations = object2array(wc.allDefinitions)
        .filter(definition => word === definition.word)
        .map(definition => definition.translations)
        .reduce((arr, currents) => {
            return arr.concat(currents);
        }, []);

    return {
        hideEmptyList: true,
        translations: Object.keys(wc.allTranslations)
            .filter(tId => definitionsTranslations.indexOf(tId) === -1)
            .filter(tId => wc.allTranslations[tId].word === wc.word)
            .map(tId => wc.allTranslations[tId])
    };
}

function mapDispatchToProps(dispatch) {
    return {
        moveTranslationToGlobals: (translationId) => {
            dispatch(actions.removeTranslationFromDefinition(translationId));
        },
        deleteTranslation: (translationId) => {
            dispatch(actions.deleteWordTranslation(translationId));
        }
    };
}

const droppableTarget = DropTarget(ItemTypes.TRANSLATION, listTarget, collect)(TranslationsList);
export default connect(mapStateToProps, mapDispatchToProps)(droppableTarget);