<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSentencesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sentences_translations', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('sentence_id');
            $table->string('text');
            $table->json('extra');

            $table->primary('id');
            $table->foreign('sentence_id')
                ->references('id')->on('sentences')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sentences_translations');
    }
}
