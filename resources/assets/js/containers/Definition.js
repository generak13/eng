import { connect } from 'react-redux';
import { ItemTypes } from '../Constants';
import { DropTarget } from 'react-dnd';
import Definition from '../components/Definition'
import * as actions from '../actions';

/* React DnD */
const listTargetForTranslations = {
    drop(props, monitor) {
        props.addDefinitionTranslation(monitor.getItem().id, props.definition.id);
    }
};

const listTargetForExamples = {
    drop(props, monitor) {
        props.addDefinitionExample(monitor.getItem().id, props.definition.id);
    }
};

function collectForTranslations(connect, monitor) {
    return {
        connectDropTargetTranslation: connect.dropTarget(),
        isTranslationOver: monitor.isOver(),
        canDropTranslation: monitor.canDrop(),
    };
}


function collectForExamples(connect, monitor) {
    return {
        connectDropTargetExample: connect.dropTarget(),
        isExampleOver: monitor.isOver(),
        canDropExample: monitor.canDrop(),
    };
}

/* Redux connect */
function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addDefinitionExample: (exampleId, definitionId) => {
            dispatch(actions.moveExampleToDefinition(exampleId, definitionId));
        },
        addDefinitionTranslation: (translationId, definitionId) => {
            dispatch(actions.moveTranslationToDefinition(translationId, definitionId));
        },
    };
}

const droppableTarget = DropTarget(ItemTypes.TRANSLATION, listTargetForTranslations, collectForTranslations)(
    DropTarget(ItemTypes.EXAMPLE, listTargetForExamples, collectForExamples)(Definition)
);

export default connect(mapStateToProps, mapDispatchToProps)(droppableTarget);