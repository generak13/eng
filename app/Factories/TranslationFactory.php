<?php

namespace App\Factories;

use App\Models\Definition;
use App\Models\Translation;

class TranslationFactory {
    public static function findOrCreate(array $data, Definition $definition): Translation {
        ['id' => $id, 'text' => $text] = $data;

        $translation = Translation::query()->find($id);

        if (!($translation instanceof Translation)) {
            $translation = new Translation();
            $translation->setId($id);
            $translation->setText($text);

            $translation->definition()->associate($definition);

            $translation->save();

        }

        return $translation;
    }
}