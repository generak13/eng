export function validateRegister(name, email, password, passwordConfirmation) {
    const errors = {};

    if (!validateName(name)) {
        errors.name = 'Name is required!';
    }

    if (!validateEmail(email)) {
        errors.email = 'Email is not valid!';
    }

    if (!validatePassword(password, passwordConfirmation)) {
        errors.password = '';
        errors.password_confirmation = 'The passwords must be not empty and identical!';
    }

    return errors;
}

function validateName(name) {
    return name.length;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePassword(password, confirm) {
    return password.length && password === confirm;
}
