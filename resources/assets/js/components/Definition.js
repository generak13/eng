import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import classNames from 'classnames';
import DefinitionTranslationsList from '../containers/DefinitionTranslationsList'
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import ExpansionPanel, {
    ExpansionPanelSummary,
    ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import ExamplesList from '../containers/ExamplesList'

export default class Definition extends Component {
    constructor() {
        super();

        this.state = {
            expanded: false,
        };
    }

    render() {
        const {
            definition,
            connectDropTargetTranslation, isTranslationOver, canDropTranslation,
            connectDropTargetExample, isExampleOver, canDropExample,
        } = this.props;

        const { expanded } = this.state;

        return (
            <ExpansionPanel
                className={this.getDefinitionBlockClasses(definition, { expanded, isOver: isTranslationOver || isExampleOver })}
                expanded={expanded}
                onChange={() => this.toggleExpanded()}>

                <ExpansionPanelSummary
                    style={{userSelect: 'auto'}}
                    expandIcon={<Icon>expand_more</Icon>}
                    ref={instance => {
                        if (!expanded) {
                            let node = ReactDOM.findDOMNode(instance);
                            connectDropTargetExample(node);
                            connectDropTargetTranslation(node);
                        }
                    }}
                >
                    <Typography>{ definition.text }</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid container>
                        <Grid item md={9}>
                            <ExamplesList definition={definition} examples={definition.examples} />
                        </Grid>
                        <Grid item md={3}>
                            <DefinitionTranslationsList definition={definition} translations={definition.translations} />
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }

    toggleExpanded() {
        this.setState(prevState => {
            return {
                expanded: !prevState.expanded,
            };
        });
    }

    getDefinitionBlockClasses(definition, { expanded, isOver }) {
        const isActive = (definition.text && definition.examples.length && definition.translations.length) ||
            (!expanded && isOver);

        return classNames('padding-small', 'margin-top-small', 'definition-block', {
            'definition-block_active': isActive,
            'definition-block_not-active': !isActive,
        });
    }
}