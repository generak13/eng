import ExampleItem from '../components/ExampleItem'
import { ItemTypes} from '../Constants';
import { connect } from 'react-redux';
import { DragSource } from 'react-dnd';
import { object2array } from '../utils/object'
import * as actions from '../actions';

/**
 * Implements the drag source contract.
 */
const translationSource = {
    beginDrag(props) {
        return props.example;
    }
};

/**
 * Specifies the props to inject into your component.
 */
function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        connectDragPreview: connect.dragPreview(),
        isDragging: monitor.isDragging()
    };
}

function mapStateToProps(state, ownProps) {
    const wc = state.wordConstructor;
    const word = wc.word;
    const example = ownProps.example;

    return {
        definitions: object2array(wc.allDefinitions)
            .filter(definition => word === definition.word)
            .filter(definition => definition.examples.indexOf(example.id) === -1)
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addDefinitionExample: (exampleId, definitionId) => {
            dispatch(actions.moveExampleToDefinition(exampleId, definitionId));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DragSource(ItemTypes.EXAMPLE, translationSource, collect)(ExampleItem));