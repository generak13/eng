<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Sentence extends Model
{
    public $incrementing = false;

    public $timestamps = false;

    protected $keyType = 'string';

    protected $fillable = [
        'text',
        'provider',
    ];

    protected $casts = [
        'text' => 'string',
        'provider' => 'string',
        'extra' => 'array',
    ];

    public function definition(): BelongsTo {
        return $this->belongsTo(Definition::class);
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                return false;
            }
        });
    }

    public function setId(string $id): Sentence {
        $this->id = $id;
        return $this;
    }

    public function setText(string $text): Sentence {
        $this->text = $text;
        return $this;
    }

    public function setProvider(string $provider): Sentence {
        $this->provider = $provider;
        return $this;
    }

    public function setTranslation(?string $translation): Sentence {
        $this->translation = $translation;
        return $this;
    }

    public function setExtra(array $extra): Sentence {
        $this->extra = $extra;
        return $this;
    }
}
