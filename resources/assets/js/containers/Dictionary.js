import React from 'react'
import { connect } from 'react-redux'
import Dictionary from '../components/dictionary'
import * as actions from '../actions'

function mapStateToProps(state) {
    const { loading, items, hasMore,
        words: allWords,
        sentences: allSentences,
        definitions: allDefinitions
    } = state.dictionary

    return {
        loading: loading,
        words: items.map(wordId => {
            const word = allWords[wordId]
            const definitions = word.definitions
                .map(definitionId => {
                    const definition = allDefinitions[definitionId]
                    return {
                        ...definition,
                        sentences: definition.sentences.map(sentenceId => allSentences[sentenceId])
                    }
                })

            return {
                ...word,
                definitions,
            }
        }),
        hasMoreWords: hasMore,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        loadDictionary: (params, options) => dispatch(actions.loadDictionary(params, options))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dictionary)