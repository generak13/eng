import { array2object } from '../utils/array'
import * as ACTION_TYPES from '../actions/types';
import {object2array} from '../utils/object';
const uuid = require('uuid/v4');

const initState = {
    liveWord: 'qq',
    liveTranslation: '',

    loading: false,

    isNew: true,
    word: 'hello',
    allDefinitions: {
        '1': {
            id: '1',
            word: 'hello',
            text: 'This is the first definition',
            examples: ['1'],
            translations: ['1'],
        }
    },
    allExamples: {
        '1': {
            id: '1',
            text: 'This is the first example',
            word: 'hello',
        }
    },
    allTranslations: {
        '1': {
            id: '1',
            text: 't1',
            word: 'hello',
        }
    },
    addedWords: [],
};

export default function (state = initState, action) {
    let nextState;
    let translationId;
    let allDefinitions;
    let allTranslations;
    let allExamples;

    switch (action.type) {
        case ACTION_TYPES.CHANGE_SEARCH_FIELD:
            return Object.assign({}, state, {
                liveWord: action.payload.value
            });
        case ACTION_TYPES.CHANGE_WORD_TRANSLATION:
            return Object.assign({}, state, {
               liveTranslation: action.payload.value
            });
        case ACTION_TYPES.SUBMIT_CHANGE_SEARCH_FIELD:
            nextState = Object.assign({}, state, {
                isNew: true,
                word: state.liveWord,
            });

            return nextState;
        case ACTION_TYPES.DEFINITION_LOADING_STARTED:
            return Object.assign({}, state, {
                loading: true
            });
        case ACTION_TYPES.DEFINITION_LOADING_FINISHED:
            return Object.assign({}, state, {
                loading: false
            });
        case ACTION_TYPES.SET_WORD_DEFINITIONS_DATA:
            const data = action.payload.data;
            const word = data.word;

            let newExamples = data.nonDefinitionSentences;
            let newTranslations = data.translations.map(translation => {
                return {
                    ...translation,
                    word,
                };
            });

            let newDefinitions = data.definitions.map(definition => {
                return {
                    id: definition.id,
                    text: definition.text,
                    word,
                    translations: definition.translations.map(translation => translation.id),
                    examples: definition.sentences.map(example => example.id),
                }
            });

            data.definitions.forEach(definition => {
                newExamples = newExamples.concat(definition.sentences);
                newTranslations = newTranslations.concat(definition.translations);
            });

            newExamples = newExamples.map(example => {
                return {
                    ...example,
                    word,
                };
            });

            nextState = Object.assign({}, state, {
                isNew: data.is_new,
                word: word,
                allDefinitions: Object.assign({}, state.allDefinitions, array2object(newDefinitions)),
                allExamples: Object.assign({}, state.allExamples, array2object(newExamples)),
                allTranslations: Object.assign({}, state.allTranslations, array2object(newTranslations)),
            });

            return nextState;
        case ACTION_TYPES.ADD_WORD_TRANSLATION:
            const id = uuid();

            const translation = {
                id,
                word: action.payload.word,
                text: action.payload.translation,
            };

            return Object.assign({}, state, {
                liveTranslation: '',
                allTranslations: Object.assign({}, state.allTranslations, {
                    [id]: translation
                })
            });
        case ACTION_TYPES.DELETE_WORD_TRANSLATION:
            allTranslations = object2array(state.allTranslations).filter(translation => translation.id !== action.payload.id);
            allDefinitions = object2array(state.allDefinitions)
                .map(definition => {
                    return Object.assign({}, definition, {
                        translations: definition.translations.filter(id => id !== action.payload.id)
                    })
                });

            return Object.assign({}, state, {
                allTranslations: array2object(allTranslations),
                allDefinitions: array2object(allDefinitions),
            });
        case ACTION_TYPES.MOVE_TRANSLATION_TO_DEFINITION:
            var definitionId = action.payload.definitionId;
            let translationId = action.payload.translationId;

            allDefinitions = object2array(state.allDefinitions).map(definition => {
                const translations = definition.translations.filter(id => id !== translationId);

                if (definition.id === definitionId) {
                    translations.push(translationId);
                }

                return Object.assign({}, definition, {
                    translations,
                });
            });

            return Object.assign({}, state, {
                allDefinitions: array2object(allDefinitions),
            });
        case ACTION_TYPES.REMOVE_TRANSLATION_FROM_DEFINITION:
            translationId = action.payload.translationId;

            allDefinitions = object2array(state.allDefinitions).map(definition => {
                return Object.assign({}, definition, {
                    translations: definition.translations.filter(id => id !== translationId),
                });
            });

            return Object.assign({}, state, {
                allDefinitions: array2object(allDefinitions),
            });
        case ACTION_TYPES.DELETE_EXAMPLE:
            allExamples = object2array(state.allExamples).filter(example => example.id !== action.payload.id);
            allDefinitions = object2array(state.allDefinitions).map(definition => {
                return Object.assign({}, definition, {
                    examples: definition.examples.filter(id => id !== action.payload.id),
                });
            });

            return Object.assign({}, state, {
                allExamples: array2object(allExamples),
                allDefinitions: array2object(allDefinitions),
            });
        case ACTION_TYPES.MOVE_EXAMPLE_TO_DEFINITION:
            var definitionId = action.payload.definitionId;
            var exampleId = action.payload.exampleId;

            allDefinitions = object2array(state.allDefinitions).map(definition => {
                const examples = definition.examples.filter(id => id !== exampleId);

                if (definition.id === definitionId) {
                    examples.push(exampleId);
                }

                return Object.assign({}, definition, {
                    examples,
                })
            });

            return Object.assign({}, state, {
                allDefinitions: array2object(allDefinitions),
            });
        case ACTION_TYPES.MOVE_EXAMPLE_TO_OTHERS:
            var exampleId = action.payload.exampleId;

            allDefinitions = object2array(state.allDefinitions).map(definition => {
                return Object.assign({}, definition, {
                    examples: definition.examples.filter(id => id !== exampleId),
                })
            });

            return Object.assign({}, state, {
                allDefinitions: array2object(allDefinitions),
            });
        case ACTION_TYPES.SUBMIT_NEW_WORD_REQUEST:
            state = Object.assign({}, state, {
                loading: true,
            });

            return state;
        case ACTION_TYPES.SUBMIT_NEW_WORD_SUCCESS:
            state = Object.assign({}, state, {
                isNew: true,
                loading: false,
                liveWord: '',
                word: '',
                addedWords: state.addedWords.concat(state.word),
            });

            return state;
        case ACTION_TYPES.SUBMIT_NEW_WORD_FAILURE:
            state = Object.assign({}, state, {
                loading: false,
            });

            return state;
        default:
            return state;
    }
}