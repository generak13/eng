<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Webpatser\Uuid\Uuid;

class Word extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        'text',
        'user_id',
    ];

    protected $casts = [
        'id' => 'string',
        'text' => 'string',
        'user_id'   => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                return false;
            }
        });
    }

    /**
     * The word owner
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The word definitions
     * @return hasMany
     */
    public function definitions(): hasMany
    {
        return $this->hasMany(Definition::class);
    }

    public function setId(string $id): Word {
        $this->id = $id;
        return $this;
    }

    public function setText(string $text): Word {
        $this->text = $text;
        return $this;
    }
}
