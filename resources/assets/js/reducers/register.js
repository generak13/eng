import * as ACTION_TYPES from '../actions/types';

const initState = {
    errors: {},
    loading: false,
};

export default function (state = initState, action) {
    switch (action.type) {
        case ACTION_TYPES.REGISTER_SEND_REQUEST:
            state = Object.assign({}, state, {
                loading: true,
            });

            return state;
        case ACTION_TYPES.REGISTER_REQUEST_SUCCESS:
            state = Object.assign({}, state, {
                loading: false,
            });

            return state;
        case ACTION_TYPES.REGISTER_REQUEST_FAILURE:
            state = Object.assign({}, state, {
                loading: false,
                errors: action.payload.errors,
            });

            return state;
        case ACTION_TYPES.REMOVE_FORM_ERROR:
            let errors = Object.assign({}, state.errors);
            delete errors[action.payload.field];

            state = Object.assign({}, state, {
                errors,
            });

            return state;
        default:
            return state;
    }
}