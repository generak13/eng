<?php

namespace App\Factories;

use App\Models\Definition;
use App\Models\Sentence;

class SentenceFactory {
    public static function findOrCreate(array $data, Definition $definition): Sentence {
        ['id' => $id, 'text' => $text, 'translation' => $translation, 'extra' => $extra] = $data;

        $sentence = Sentence::query()->find($id);

        if (!($sentence instanceof Sentence)) {
            $sentence = new Sentence();
            $sentence->setId($id);
            $sentence->setText($text);
            $sentence->setProvider('custom');
            $sentence->setTranslation($translation);
            $sentence->setExtra($extra);

            $sentence->definition()->associate($definition);

            $sentence->save();
        }

        return $sentence;
    }
}