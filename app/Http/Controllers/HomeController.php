<?php

namespace App\Http\Controllers;

use App\Services\Context\ReversoContext;
use App\Services\Dictionary\CambridgeDictionary;
use Illuminate\Http\Request;
use Stichoza\GoogleTranslate\TranslateClient;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test(Request $request)
    {
        $word = $request->get('word');

        $dict = new CambridgeDictionary();
        $dict->loadData($word);

        $reverso = new ReversoContext();
        $reverso->loadData($word);

        return [
            'word' => $word,
            'def2examples' => $dict->getDefinitions2Examples(),
            'otherExamples' => $reverso->getSamples(),
            'translations' => [TranslateClient::translate('en', 'uk', $word)]
        ];
    }

    public function app()
    {
        return view('layouts.main');
    }
}
