import { array2object } from '../utils/array'
import * as ACTION_TYPES from "../actions/types";
import { pick, flatten, shuffle } from 'lodash'

const initState = {
    loading: false,
    trainingCount: localStorage.getItem('trainingCount') ? localStorage.getItem('trainingCount') : 'all',
    sentencesOrder: [],
    allDefinitions: {},
    allTranslations: {},
    allSentences: {},
};

export default function (state = initState, action) {
    let nextState;

    switch (action.type) {
        case ACTION_TYPES.TRAINING_REQUEST:
            nextState = Object.assign({}, state, {
                loading: true,
            });

            return nextState;
        case ACTION_TYPES.TRAINING_FAILURE:
            nextState = Object.assign({}, state, {
                loading: false,
            });

            return nextState;
        case ACTION_TYPES.TRAINING_SUCCESS:
            const words = action.payload.words;

            const definitions = words.map(word => {
                return {
                    ...pick(word.definition, ['id', 'text']),
                    word: word.text,
                    sentences: [word.definition.sentence.id],
                    translations: word.definition.word_translations.map(translation => translation.id),
                }
            })

            const sentences = words.map(word => {
                return {
                    ...pick(word.definition.sentence, ['id', 'text', 'translation']),
                    word: word.text,
                }
            })

            const translations = flatten(words.map(word => word.definition.word_translations.map(translation => {
                return {
                    ...pick(translation, ['id', 'text']),
                    word: word.text,
                }
            })))

            nextState = Object.assign({}, state, {
                loading: false,
                allDefinitions: array2object(definitions),
                allTranslations: array2object(translations),
                allSentences: array2object(sentences),
                sentencesOrder: shuffle(sentences.map(sentence => sentence.id)),
            });

            return nextState;
        case ACTION_TYPES.SET_TRAINING_COUNT:
            nextState = Object.assign({}, state, {
                trainingCount: action.payload.count,
            });

            localStorage.setItem('trainingCount', action.payload.count);

            return nextState;
        default:
            return state;
    }
}