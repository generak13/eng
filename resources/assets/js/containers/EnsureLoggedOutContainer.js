import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux'

class EnsureLoggedOutContainer extends React.Component {
    componentDidMount() {
        const { dispatch, isLoggedIn } = this.props;

        if (isLoggedIn) {
            dispatch(push('/logout'));
        }
    }

    render() {
        if (this.props.isLoggedIn) {
            return null;
        } else {
            return this.props.children;
        }
    }
}

// Grab a reference to the current URL. If this is a web app and you are
// using React Router, you can use `ownProps` to find the URL. Other
// platforms (Native) or routing libraries have similar ways to find
// the current position in the app.
function mapStateToProps(state, ownProps) {
    return {
        isLoggedIn: state.user.isLoggedIn
    }
}

export default connect(mapStateToProps)(EnsureLoggedOutContainer)