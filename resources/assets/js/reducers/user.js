import axios from 'axios';
import * as ACTION_TYPES from '../actions/types';

const initState = {
    isLoggedIn: !!localStorage.getItem('access_token'),
    redirectUrl: '/',
};

if (initState.isLoggedIn) {
    axios.defaults.headers.common['Authorization'] = getAuthorizationHeader(localStorage.getItem('access_token'));
}

function getAuthorizationHeader(accessToken) {
    return 'Bearer ' + accessToken;
}

export default function (state = initState, action) {
    switch (action.type) {
        case ACTION_TYPES.TOKEN_REQUEST_SUCCESS:
            state = Object.assign({}, state, {
                isLoggedIn: !!action.payload.data.access_token,
            });

            localStorage.setItem('access_token', action.payload.data.access_token);
            axios.defaults.headers.common['Authorization'] = getAuthorizationHeader(action.payload.data.access_token);

            return state;
        case ACTION_TYPES.TOKEN_REQUEST_FAILURE:
            state = Object.assign({}, state, {
                isLoggedIn: false,
            });

            return state;
        case ACTION_TYPES.REMOVE_TOKEN:
            state = Object.assign({}, state, {
                isLoggedIn: false,
            });

            localStorage.removeItem('access_token');
            delete axios.defaults.headers.common['Authorization'];

            return state;
        case ACTION_TYPES.SET_REDIRECT_URL:
            state = Object.assign({}, state, {
                redirectUrl: action.payload.url,
            });

            return state;
        default:
            return state;
    }
}