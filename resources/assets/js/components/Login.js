import React, { Component } from 'react';
import { Link } from "react-router";
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import { LinearProgress } from 'material-ui/Progress';

export default class Login extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            wrongCredentials: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.wrongCredentials !== this.state.wrongCredentials) {
            this.setState({
                wrongCredentials: nextProps.wrongCredentials,
            });
        }
    }

    render() {
        return (
            <Paper elevation={4} className={'login-wrapper'}>
                <Typography variant="headline" component="h3">
                    Enter your credentials
                </Typography>

                {
                    this.props.wrongCredentials && (
                        <Typography variant="headline" component="h4" color={'error'}>
                            Credentials is wrong
                        </Typography>
                    )
                }

                <form noValidate autoComplete="off">
                    <div>
                        <TextField
                            label="Email"
                            value={this.state.email}
                            margin='normal'
                            onChange={(e) => this.onChange(e, 'email')}
                            onKeyPress={(e) => this.catchReturn(e)}
                        />
                    </div>
                    <div>
                        <TextField
                            id="password-input"
                            label="Password"
                            value={this.state.password}
                            type="password"
                            autoComplete="current-password"
                            margin="normal"
                            onChange={(e) => this.onChange(e, 'password')}
                            onKeyPress={(e) => this.catchReturn(e)}
                        />
                    </div>
                    <Button variant="raised" color="primary" className={'login-submit'} onClick={() => this.processSubmit()}>Login In</Button>

                    <div>
                        or you free to <Link to={'/register'}>Register the new account</Link>
                    </div>
                </form>

                { this.props.loading && <LinearProgress /> }
            </Paper>
        );
    }

    onChange(e, field) {
        this.setState({
            [field]: e.target.value
        });

        if (this.state.wrongCredentials) {
            this.props.flushWrongCredentials();
        }
    }

    catchReturn(e) {
        if (e.key === 'Enter') {
            this.processSubmit();

            e.preventDefault();
        }
    }

    processSubmit() {
        const { email, password } = this.state;

        this.props.submit(email, password);
    }
}