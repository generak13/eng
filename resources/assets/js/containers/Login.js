import { connect } from 'react-redux';
import Login from '../components/Login'
import * as actions from '../actions';

/* Redux connect */
function mapStateToProps(state) {
    const sl = state.login;

    return {
        loading: sl.loading,
        wrongCredentials: sl.wrongCredentials,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        submit: (email, password) => dispatch(actions.recieveToken(email, password)),
        flushWrongCredentials: () => dispatch(actions.loginFlushWrongCredentials()),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);