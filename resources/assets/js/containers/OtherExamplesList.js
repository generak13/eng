import OtherExamplesList from '../components/OtherExamplesList'
import { connect } from 'react-redux';
import { ItemTypes } from '../Constants';
import { DropTarget } from 'react-dnd';
import * as actions from '../actions';

/* React DnD */
const listTarget = {
    drop(props, monitor) {
        props.removeExampleFromDefinitions(monitor.getItem().id);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
    };
}


/* Redux connect */
function mapStateToProps(state) {
    let wc = state.wordConstructor;
    let definitionExamplesIds = [].concat.apply([], (Object.values(wc.allDefinitions).map(definition => definition.examples)));

    return { //receive props from parent
        examples: Object.values(wc.allExamples)
            .filter(example => example.word === wc.word)
            .filter(example => definitionExamplesIds.indexOf(example.id) === -1)
    };
}

function mapDispatchToProps(dispatch) {
    return {
        removeExampleFromDefinitions: (exampleId) => {
            dispatch(actions.moveExampleToOthers(exampleId));
        },
        deleteExample: (exampleId) => {
            dispatch(actions.deleteExample(exampleId));
        }
    };
}

const droppableTarget = DropTarget(ItemTypes.EXAMPLE, listTarget, collect)(OtherExamplesList);
export default connect(mapStateToProps, mapDispatchToProps)(droppableTarget);