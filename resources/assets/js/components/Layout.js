import React from 'react'
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import { MenuList, MenuItem } from 'material-ui/Menu';

const drawerWidth = 300;

const styles = theme => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        display: 'flex',
        zIndex: 1,
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    content: {
        flexGrow: 1,
        // backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
});

const menus = [{
    id: 'register',
    icon: 'account_circle',
    label: 'Register',
    pathname: '/register',
    show: ({isLoggedIn}) => !isLoggedIn
}, {
    id: 'login',
    icon: 'lock_open',
    label: 'Login',
    pathname: '/login',
    show: ({isLoggedIn}) => !isLoggedIn
}, {
    id: 'training',
    icon: 'rowing',
    label: 'Training',
    pathname: '/training',
    show: ({isLoggedIn}) => isLoggedIn
}, {
    id: 'video',
    icon: 'video_library',
    label: 'Video',
    pathname: '/video',
    show: ({isLoggedIn}) => isLoggedIn
}, {
    id: 'word-constructor',
    icon: 'build',
    label: 'Word Constructor',
    pathname: '/constructor',
    show: ({isLoggedIn}) => isLoggedIn
}, {
    id: 'dictionary',
    icon: 'book',
    label: 'Dictionary',
    pathname: '/dictionary',
    show: ({isLoggedIn}) => isLoggedIn,
}, {
    id: 'logout',
    icon: 'lock',
    label: 'Logout',
    pathname: '/logout',
    show: ({isLoggedIn}) => isLoggedIn
}];

class Layout extends React.Component {
    constructor() {
        super();

        this.state = {
            open: false,
            anchor: 'left',
        };
    }

    handleDrawerOpen() {
        this.setState({ open: true });
    };

    handleDrawerClose() {
        this.setState({ open: false });
    };

    onMenuSelected(item) {
        this.props.selectMenuItem(item);

        this.handleDrawerClose();
    }

    render() {
        const { classes, theme } = this.props;
        const { anchor, open } = this.state;

        const drawer = (
            <Drawer
                variant="persistent"
                anchor={anchor}
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton  onClick={this.handleDrawerClose.bind(this)}>
                        <Icon>chevron_left</Icon>
                    </IconButton>
                </div>
                <Divider />
                <MenuList>
                    {menus.map((item) =>
                        item.show({ isLoggedIn: this.props.isLoggedIn }) &&
                            <MenuItem
                                key={item.id}
                                selected={item.pathname === this.props.location.pathname}
                                onClick={() => this.onMenuSelected(item)}
                            >
                            <ListItemIcon>
                                <Icon>{item.icon}</Icon>
                            </ListItemIcon>
                            <ListItemText inset primary={item.label} />
                        </MenuItem>
                    )}
                </MenuList>
            </Drawer>
        );

        return (
            <div className={classes.root}>
                <AppBar
                        className={classNames(classes.appBar, {
                            [classes.appBarShift]: open,
                            [classes[`appBarShift-${anchor}`]]: open,
                        })}
                >
                    <Toolbar disableGutters={false}>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.handleDrawerOpen.bind(this)}
                            className={classNames(classes.menuButton, open && classes.hide)}
                        >
                            <Icon>menu</Icon>
                        </IconButton>
                        <Typography variant="title" color="inherit" noWrap>
                            {menus.find(item => item.pathname === this.props.location.pathname).label}
                        </Typography>
                    </Toolbar>
                </AppBar>
                { drawer }
                <main
                    className={classNames(classes.content, classes[`content-${anchor}`], {
                        [classes.contentShift]: open,
                        [classes[`contentShift-${anchor}`]]: open,
                    })}
                >
                    <div className={classes.drawerHeader} />

                    {this.props.children}
                </main>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(Layout);
