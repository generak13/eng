import * as ACTION_TYPES from '../actions/types';
import { array2object } from "../utils/array"
import { uniq, pick, flatten } from 'lodash'

const initState = {
    items: [],
    loading: false,
    hasMore: true,
    words: {},
    definitions: {},
    sentences: {},
};

const recordsChunkCount = 15

export default function (state = initState, action) {
    switch (action.type) {
        case ACTION_TYPES.DICTIONARY_REQUEST: {
            const nextState = Object.assign({}, state, {
                loading: true,
            })

            return nextState
        }
        case ACTION_TYPES.DICTIONARY_FAILURE: {
            const nextState = Object.assign({}, state, {
                loading: false,
            })

            return nextState
        }
        case ACTION_TYPES.DICTIONARY_SUCCESS: {
            const wordsData = action.payload.words
            const options = action.payload.options

            const words = wordsData.map(word => {
                return {
                    ...pick(word, ['id', 'text', 'created_at', 'updated_at']),
                    definitions: word.definitions.map(definition => definition.id),
                }
            })

            const definitions = flatten(wordsData.map(({definitions}) => definitions.map(definition => {
                return {
                    ...pick(definition, ['id', 'text']),
                    sentences: definition.sentences.map(sentence => sentence.id)
                }
            })))

            const sentences = flatten(wordsData.map(({definitions}) => flatten(definitions.map(({sentences}) => sentences))))

            const newItems = words.map(word => word.id)
            const items = options.replace ? newItems : uniq(state.items.concat(newItems))

            const nextState = Object.assign({}, state, {
                items,
                words: Object.assign({}, state.words, array2object(words)),
                definitions: Object.assign({}, state.definitions, array2object(definitions)),
                sentences: Object.assign({}, state.sentences, array2object(sentences)),
                loading: false,
                hasMore: wordsData.length === recordsChunkCount,
            })

            return nextState
        }
        default:
            return state;
    }
}