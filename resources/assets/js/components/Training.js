import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withStyles } from 'material-ui/styles';
import classnames from 'classnames';
import { Link } from "react-router";
import SwipeableViews from 'react-swipeable-views';
import { bindKeyboard } from 'react-swipeable-views-utils';
import EventListener, {withOptions} from 'react-event-listener';
import Pagination from './Pagination';
import Card, { CardContent, CardActions } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Collapse from 'material-ui/transitions/Collapse';
import Radio from 'material-ui/Radio/Radio';
import RadioGroup from 'material-ui/Radio/RadioGroup';
import FormHelperText from 'material-ui/Form/FormHelperText';
import FormControlLabel from 'material-ui/Form/FormControlLabel';
import FormControl from 'material-ui/Form/FormControl';
import FormLabel from 'material-ui/Form/FormLabel'
import { LinearProgress } from 'material-ui/Progress';
import Player from "./Player";

const styles = theme => ({
    wrapper: {
        maxWidth: '50%',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    cardWrapper: {
        position: 'relative',
    },
    card: {
    },
    actions: {
        display: 'flex',
    },
    title: {
        marginBottom: 16,
        fontSize: 14,
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    trainingReload: {
        position: 'absolute',
        top: 5,
        right: -35,
        cursor: 'pointer',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    trainingReloadOver: {
        transform: 'rotate(360deg)',
    },
    hidden: {
        display: 'none',
    },
    radioGroup: {
        flexDirection: 'row',
    }
});

const BindKeyboardSwipeableViews = bindKeyboard(SwipeableViews);
const playNextTimeout = 1000
const watsonUrl = 'https://watson-api-explorer.ng.bluemix.net/text-to-speech/api/v1/synthesize?accept=audio%2Fogg%3Bcodecs%3Dopus&voice=en-US_MichaelVoice&text='

const BUTTON_UP = 38
const BUTTON_LEFT = 37
const BUTTON_RIGHT = 39
const BUTTON_DOWN = 40
const BUTTON_SPACE = 32

class Training extends Component {
    constructor() {
        super()
        this.state = {
            index: 0,
            expanded: false,
            overReload: false,
            audioRegime: false,
            autoPlay: false,
            play: false,
        }

        this.autoPlayTimer = null
    }

    componentDidMount() {
        this.props.loadTraining({count: this.props.trainingCount});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ index: 0 });
    }

    componentDidUpdate(prevProps) {
        if (this.props.trainingCount !== prevProps.trainingCount) {
            this.reloadTraining();
        }
    }

    reloadTraining() {
        this.setState({
            play: false,
        })

        this.props.loadTraining({count: this.props.trainingCount});
    }

    toggleExpand() {
        this.setState({ expanded: !this.state.expanded });
    };

    handleKeys(event) {
        if (~[BUTTON_UP, BUTTON_LEFT, BUTTON_RIGHT].indexOf(event.keyCode)) {
            this.setState({ expanded: false });
        } else if (event.keyCode === BUTTON_DOWN) {
            this.setState({ expanded: true });
        } else if (event.keyCode === BUTTON_SPACE && event.shiftKey) {
            this.play(this.props.sentences[this.state.index].word);
        }
    }

    handleChangeIndex(index) {
        this.stopPlayer();
        this.handleChangeCard(index);
    }

    handleChangeCard(index) {
        const currentIndex = this.state.index;

        const isFirstPage = page => page === 0;
        const isLastPage = page => page === this.props.sentences.length - 1;

        if (isLastPage(currentIndex) && isFirstPage(index)) { // do not circle
            this.reloadTraining();
        } else if (!(isFirstPage(currentIndex) && isLastPage(index))) {
            this.setState({
                index,
            });
        }
    }

    stopPlayer() {
        this.setState({play: false})

        if (this.autoPlayTimer) {
            clearTimeout(this.autoPlayTimer)
            this.autoPlayTimer = null
        }
    }

    mouseOverReload(e) {
        this.setState({
            overReload: true,
        });
    }

    mouseLeaveReload(e) {
        this.setState({
            overReload: false,
        });
    }

    play(text) {
        new Audio(this.getSpeechUrl(text)).play();
    }

    playNext() {
        this.setState({
            play: false,
        })

        if (this.state.autoPlay) {
            this.autoPlayTimer = setTimeout(() => {
                ReactDOM.unstable_batchedUpdates(() => {
                    const nextIndex = this.state.index + 1 === this.props.sentences.length ? 0 : this.state.index + 1;
                    this.handleChangeCard(nextIndex);
                    this.setState({
                        play: true,
                    });
                });
            }, playNextTimeout);
        }
    }

    handleErrorPlay() {
        this.setState({
            play: false,
        })

        this.autoPlayTimer = setTimeout(() => {
            ReactDOM.unstable_batchedUpdates(() => {
                this.setState({
                    play: true,
                });
            });
        }, playNextTimeout);
    }

    getSpeechUrl(text) {
        return watsonUrl + encodeURI(text)
    }

    changeAutoPlay(flag) {
        this.setState({
            autoPlay: flag,
        })
    }

    changePlay(play) {
        this.setState({
            play,
        })
    }

    changeTrainingCount(e) {
        this.props.setTrainingCount(e.target.value);
    }

    render() {
        const { classes, loading, trainingCount } = this.props
        let { sentences } = this.props
        const { index } = this.state;

        let content = null;

        if (!sentences.length) {
            content = (
                <div style={{textAlign: 'center'}}>
                    You don't have any words to learn. Add them at first <Link to="/constructor">here</Link>
                </div>
            );
        } else {
            sentences = loading ? [] : sentences;

            content = (
                <div>
                    <LinearProgress className={classnames({[classes.hidden]: !loading})}/>
                    <div className={classnames(classes.wrapper, {
                        [classes.hidden]: loading,
                    })}>
                        <FormControl component="fieldset" className={classes.formControl}>
                            <FormLabel component="legend">Train last words</FormLabel>
                            <RadioGroup
                                name="training-count"
                                className={classes.radioGroup}
                                value={trainingCount}
                                onChange={(e) => this.changeTrainingCount(e)}
                            >
                                <FormControlLabel value="20" control={<Radio />} label="20" />
                                <FormControlLabel value="50" control={<Radio />} label="50" />
                                <FormControlLabel value="100" control={<Radio />} label="100" />
                                <FormControlLabel value="all" control={<Radio />} label="All" />
                            </RadioGroup>
                        </FormControl>

                        <EventListener target='window' onKeyDown={(e) => this.handleKeys(e)}>
                            <div className={classnames(classes.cardWrapper)}>
                                <BindKeyboardSwipeableViews enableMouseEvents index={index} onChangeIndex={(index) => this.handleChangeIndex(index)} className={classes.card}>
                                    {
                                        sentences.map(sentence =>
                                            <Card key={sentence.id}>
                                                <CardContent>
                                                    <Typography className={classes.title} color="textSecondary">
                                                        <IconButton onClick={() => this.play(sentence.word)}>
                                                            <Icon>play_circle_filled</Icon>
                                                        </IconButton>
                                                        <span>{`${sentence.word}: ${sentence.translations.map(translation => translation.text).join(', ')}`}</span>
                                                    </Typography>
                                                    <Typography component="p" variant="title">
                                                        { sentence.text }
                                                    </Typography>
                                                </CardContent>
                                                <CardActions disableActionSpacing>
                                                    <IconButton
                                                        className={classnames(classes.expand, {
                                                            [classes.expandOpen]: this.state.expanded,
                                                        })}
                                                        onClick={() => this.toggleExpand()}
                                                        aria-expanded={this.state.expanded}
                                                        aria-label="Show more"
                                                    >
                                                        <Icon>expand_more</Icon>
                                                    </IconButton>
                                                </CardActions>
                                                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                                                    <CardContent>
                                                        {sentence.translation &&
                                                        <div>
                                                            <Typography paragraph color="textSecondary">
                                                                { sentence.translation }
                                                            </Typography>
                                                            <Divider/>
                                                        </div>
                                                        }
                                                        <Typography paragraph variant="body2">
                                                            { sentence.definition.text }
                                                        </Typography>
                                                    </CardContent>
                                                </Collapse>
                                            </Card>
                                        )
                                    }
                                </BindKeyboardSwipeableViews>
                                <Pagination dots={sentences.length} index={index} onChangeIndex={this.handleChangeIndex.bind(this)} />

                                <IconButton
                                    className={classnames(classes.trainingReload, {
                                        [classes.trainingReloadOver]: this.state.overReload,
                                    })}
                                    onMouseOver={(e) => this.mouseOverReload(e)}
                                    onMouseLeave={(e) => this.mouseLeaveReload(e)}
                                    onClick={(e) => this.reloadTraining(e)}
                                >
                                    <Icon>loop</Icon>
                                </IconButton>
                            </div>
                        </EventListener>

                        <Player
                            play={this.state.play && sentences.length}
                            playStopClb={(play) => this.changePlay(play)}
                            url={sentences.length ? this.getSpeechUrl(sentences[this.state.index].text) : null}
                            onended={() => this.playNext()}
                            onerror={() => this.handleErrorPlay()}
                            autoPlay={this.state.autoPlay}
                            autoPlayClb={(flag) => this.changeAutoPlay(flag)}/>
                    </div>
                </div>
            );
        }

        return (
            <div>{content}</div>
        );
    }
}

export default withStyles(styles)(Training);