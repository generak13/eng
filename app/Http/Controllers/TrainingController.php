<?php

namespace App\Http\Controllers;

use App\Models\Sentence;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TrainingController extends Controller
{
    public function index(Request $request)
    {
        $wordsLimit = 10;
        $lastWordsCount = $request->get('count', 'all');

        $dateFrom = null;

        if ($lastWordsCount !== 'all') {
            $words = Word::query()
                ->orderBy('created_at', 'DESC')
                ->take((int)$lastWordsCount)
                ->get();

            if ($words->count()) {
                $dateFrom = $words->last()->created_at;
            }
        }

        $query = Word::with('definitions', 'definitions.sentences', 'definitions.wordTranslations')
            ->inRandomOrder()
            ->take($wordsLimit);

        if ($dateFrom) {
            $query->where('created_at', '>=', $dateFrom);
        }

        $words = $query->get();

        $words = $words->map(function (Word $word) {
            $word->definition = $word->definitions->random();
            $word->definition->sentence = $word->definition->sentences->random();

            unset($word->definition->sentences);
            unset($word->definitions);

            return $word;
        });

        return response()->json([
            'words' => $words,
        ], Response::HTTP_OK);
    }
}
