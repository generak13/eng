import { connect } from 'react-redux';
import Register from '../components/Register'
import * as actions from '../actions';

/* Redux connect */
function mapStateToProps(state) {
    const sr = state.register;
    const sl = state.login;

    return {
        loading: sr.loading || sl.loading,
        errors: sr.errors,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        submit: (name, email, password, passwordConfirmation) => dispatch(actions.register(name, email, password, passwordConfirmation)),
        removeFormError: (field) => dispatch(actions.removeFormError(field)),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);