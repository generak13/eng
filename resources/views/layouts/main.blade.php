<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script>
        const CLIENT_ID = '{{ env('CLIENT_ID') }}';
        const CLIENT_SECRET = '{{ env('CLIENT_SECRET') }}';
    </script>
</head>
<body>
    <div id="example">

    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>