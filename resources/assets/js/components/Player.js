import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import EventListener from 'react-event-listener'
import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import { LinearProgress } from 'material-ui/Progress'
import FormControlLabel from 'material-ui/Form/FormControlLabel'
import Checkbox from 'material-ui/Checkbox'
import Paper from 'material-ui/Paper'

const styles = theme => ({
    player: {
        display: 'flex',
        alignItems: 'center',
    },
    progress: {
        flexGrow: 1,
        marginRight: 10,
    }
})

const BUTTON_SPACE = 32

class Player extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isPlaying: false,
            progress: 0,
            error: false,
        };

        this.audio = null
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.play && nextProps.play && nextProps.url) {
            this.play(nextProps.url)
        } else if (!nextProps.play) {
            this.pause()
        }
    }

    play(url) {
        const { onended, onpause, onerror } = this.props

        if (this.audio && !this.audio.paused) {
            this.audio.pause()
        }

        this.audio = new Audio(url)

        this.audio.loadstart = () => {
            this.setState({error: false})
        }

        this.audio.onended = () => {
            onended && onended()
        };

        this.audio.onpause = () => {
            this.audio = null
            this.setState({progress: 0})
            onpause && onpause()
        };

        this.audio.ontimeupdate = () => {
            this.setState({progress: this.getProgressPercentage()})
        };

        this.audio.onerror = () => {
            this.setState({error: true})
            onerror && onerror();
        }

        this.audio.play()
    }

    pause() {
        if (this.audio) {
            this.audio.pause()
        }
    }

    getProgressPercentage() {
        if (!this.audio) {
            return 0
        }

        return this.audio.currentTime * 100 / this.audio.duration
    }

    handleKeys(event) {
        if (event.keyCode === BUTTON_SPACE && !event.shiftKey) { // space without SHIFT
            this.props.playStopClb(!this.props.play)
        }
    }

    render() {
        const { play, autoPlay, autoPlayClb, playStopClb, classes } = this.props

        return (
            <EventListener target='window' onKeyDown={(e) => this.handleKeys(e)}>
                <Paper className={classes.player}>
                    <IconButton onClick={() => playStopClb(!play)}>
                        <Icon>{ play ? 'stop' : 'play_arrow'}</Icon>
                    </IconButton>
                    <LinearProgress className={classes.progress} variant="determinate" value={this.state.progress}/>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={!!autoPlay}
                                onChange={(e, val) => autoPlayClb && autoPlayClb(val)}
                                value="autoplay"
                            />
                        }
                        label="Auto play"
                    />
                </Paper>
            </EventListener>
        )
    }
}

export default withStyles(styles)(Player);