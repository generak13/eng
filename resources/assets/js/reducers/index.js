import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import register from './register';
import login from './login';
import user from './user';
import training from './training';
import dictionary from './dictionary';
import wordConstructor from './wordConstructor';

const rootReducer = combineReducers({
    routing: routerReducer,
    register,
    login,
    user,
    training,
    dictionary,
    wordConstructor,
});

export default rootReducer;