<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/oauth/register', 'Auth\\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/training', '\App\Http\Controllers\TrainingController@index');
    Route::get('/constructor', '\App\Http\Controllers\WordConstructor@load');
    Route::get('/dictionary', '\App\Http\Controllers\DictionaryController@load');
    Route::post('/constructor', '\App\Http\Controllers\WordConstructor@save');
});