import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import TextField from 'material-ui/TextField';


class SearchWord extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 1
        };
    }

    render() {
        return (
            <form noValidate autoComplete="off" onSubmit={this.props.submitSearch}>
                <TextField
                    id="search-word"
                    label="Search Word"
                    type="search"
                    value={this.props.word}
                    onChange={this.props.searchHandler}
                    className={'search-block'}
                    margin="normal"
                />
            </form>
        );
    }
}

function mapStateToProps(state) {
    return {
        word: state.wordConstructor.liveWord,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        submitSearch: (e) => {
            e.preventDefault();

            dispatch(actions.fetchDefinitions());
        },
        searchHandler: (e) => {
            e.preventDefault();

            dispatch(actions.changeSearchField(e.target.value));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchWord);