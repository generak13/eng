import React, { Component } from 'react';
import classNames from 'classnames';
import List from 'material-ui/List';
import TranslationItem from '../containers/TranslationItem'

export default class TranslationsList extends Component {
    render() {
        const { connectDropTarget, isOver, canDrop, hideEmptyList, translations, deleteTranslation } = this.props;

        if (hideEmptyList && !translations.length) {
            return null;
        }

        return connectDropTarget(
            <div className={classNames('translation-list', {
                'translation-list_drag-over': isOver,
                'translation-list_can-drop': !isOver && canDrop,
            })}>
                <List>
                    {
                        translations.map(translation => {
                            return <TranslationItem key={translation.id} translation={translation} delete={deleteTranslation} />
                        })
                    }
                </List>
            </div>
        );
    }
}