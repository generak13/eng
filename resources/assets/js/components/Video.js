import React, { Component } from 'react';
import EventListener, {withOptions} from 'react-event-listener';
import TextField from 'material-ui/TextField';

const BUTTON_LEFT = 37
const BUTTON_RIGHT = 39
const BUTTON_SAVE = 83 // S
const BUTTON_AGAIN = 65 // A

const DELTA_TIME_REWIND = 5

class Video extends Component {
    constructor() {
        super()

        this.state = {
            src: '',
            typingSrc: '',
            rememberedPosition: 0,
        }
    }

    changeSrc(e) {
        this.setState({typingSrc: e.target.value})
    }

    submitVideo(e) {
        e.preventDefault()
        
        this.setState({src: this.state.typingSrc})
    }

    handleKeys(e) {
        switch(e.keyCode) {
            case BUTTON_LEFT:
                this.video.currentTime -= DELTA_TIME_REWIND
                break
            case BUTTON_RIGHT:
                this.video.currentTime += DELTA_TIME_REWIND
                break
            case BUTTON_SAVE:
                this.setState({rememberedPosition: this.video.currentTime})
                break
            case BUTTON_AGAIN:
                this.video.currentTime = this.state.rememberedPosition
                break
        }
    }

    render() {
        return (
            <div>
                <EventListener target='window' onKeyDown={(e) => this.handleKeys(e)}>
                    <form noValidate autoComplete="off" onSubmit={(e) => this.submitVideo(e)}>
                        <TextField
                            id="video-url"
                            label="Video URL"
                            value={this.state.typingSrc}
                            onChange={(e) => this.changeSrc(e)}
                            margin="normal"
                            fullWidth
                        />
                    </form>
                    <video
                        src={this.state.src}
                        controls
                        ref={video => this.video = video}
                    >
                    </video>
                </EventListener>
                <ul>
                    Instructions
                    <li><b>-&gt;</b> +5 seconds</li>
                    <li><b>&lt;-</b> -5 seconds</li>
                    <li><b>s</b> remember position</li>
                    <li><b>a</b> go to the remembered position</li>
                </ul>
            </div>
        )
    }
}

export default Video;