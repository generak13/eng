<?php

namespace App\Http\Controllers;

use App\Factories\DefinitionFactory;
use App\Factories\SentenceFactory;
use App\Factories\TranslationFactory;
use App\Factories\WordFactory;
use App\Models\Definition;
use App\Models\Sentence;
use App\Models\Translation;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\Context\ReversoContext;
use App\Services\Dictionary\CambridgeDictionary;
use Stichoza\GoogleTranslate\TranslateClient;
use Webpatser\Uuid\Uuid;

class WordConstructor extends Controller
{
    public function load(Request $request)
    {
        $user = auth()->user();
        $requestedWord = $request->get('word');
        $result = [
            'is_new' => true,
            'word' => $requestedWord,
            'definitions' => [],
            'nonDefinitionSentences' => [],
            'translations' => [],
        ];

        $word = Word::query()
            ->with('definitions', 'definitions.wordTranslations', 'definitions.sentences')
            ->where('text', $requestedWord)
            ->where('user_id', $user->id)
            ->first();

        $extractedData = [
            'data' => [],
            'definitions' => [],
            'sentences' => [],
            'translations' => [],
        ];

        if ($word instanceof Word) {
            $extractedData = $this->extractExistedDefinitionData($word->definitions);

            $result['is_new'] = false;
            $result['definitions'] = $extractedData['data'];
        }

        $dict = new CambridgeDictionary();
        $dict->loadData($requestedWord);
        $def2examples = $dict->getDefinitions2Examples();

        $reverso = new ReversoContext();
        $reverso->loadData($requestedWord);
        $otherExamples = $reverso->getSamples();

        [
            'definition2sentences' => $definition2sentences,
            'nonDefinitionSentences' => $nonDefinitionSentences
        ] = $this->filterByExtractedData($def2examples, $otherExamples, $extractedData);

        foreach ($definition2sentences as $definitionContent => $sentences) {
            $sentences = array_map(function($sentence) {
                return [
                    'id' => (string)Uuid::generate(4),
                    'text' => $sentence,
                    'translation' => '',
                    'highlights' => [],
                ];
            }, $sentences);

            $result['definitions'][] = [
                'id' => (string)Uuid::generate(4),
                'text' => $definitionContent,
                'sentences' => $sentences,
                'translations' => [],
            ];
        }

        $result['nonDefinitionSentences'] = array_map(function($sentence) use ($nonDefinitionSentences) {
            return [
                'id' => (string)Uuid::generate(4),
                'text' => $sentence,
                'translation' => $nonDefinitionSentences[$sentence]['translation'] ?: null,
                'highlights' => $nonDefinitionSentences[$sentence]['highlights'] ?? [],
            ];
        }, array_keys($nonDefinitionSentences));

        $translations = [TranslateClient::translate('en', 'uk', $requestedWord)];
        $translations = array_diff($translations, $extractedData['translations']);
        $result['translations'] = array_map(function($translation) {
            return [
                'id' => (string)Uuid::generate(4),
                'text' => $translation,
            ];
        }, $translations);

        return response()->json($result, Response::HTTP_OK);
    }

    private function extractExistedDefinitionData($definitions): array
    {
        $result = [
            'data' => [],
            'definitions' => [],
            'sentences' => [],
            'translations' => [],
        ];

        foreach ($definitions as $definition) {
            $tmp = [
                'id' => $definition->id,
                'text' => $definition->text,
                'sentences' => [],
                'translations' => [],
            ];

            if ($definition->sentences) {
                $tmp['sentences'] = $definition->sentences->map(function($sentence) {
                    return [
                        'id' => $sentence->id,
                        'text' => $sentence->text,
                        'translation' => $sentence->translation ?: '',
                        'highlights' => $sentence->extra['highlights'] ?? [],
                    ];
                })->toArray();
            }

            if ($definition->wordTranslations) {
                $tmp['translations'] = $definition->wordTranslations->map(function($translation) {
                    return [
                        'id' => $translation->id,
                        'text' => $translation->text,
                    ];
                })->toArray();
            }

            $result['data'][] = $tmp;
            $result['definitions'][] = $definition->text;
            $result['sentences'] = array_merge($result['sentences'], array_pluck($tmp['sentences'], 'text'));
            $result['translations'] = array_merge($result['translations'], array_pluck($tmp['translations'], 'text'));
        }

        return $result;
    }

    private function filterByExtractedData(array $definitions2sentences, array $nonDefinitionSentences2Info, array $extractedData) {
        foreach ($definitions2sentences as $definitionContent => $sentences) {
            $unassignedSentences = array_diff($sentences, $extractedData['sentences']);

            if (in_array($definitionContent, $extractedData['definitions'])) {
                unset($definitions2sentences[$definitionContent]);
                foreach ($unassignedSentences as $sentence) {
                    $nonDefinitionSentences2Info[$sentence] = [
                        'translation' => '',
                        'highlights' => [],
                    ];
                }
            } else {
                $definitions2sentences[$definitionContent] = $unassignedSentences;
            }
        }

        foreach ($extractedData['sentences'] as $sentence) {
            if (isset($nonDefinitionSentences2Info[$sentence])) {
                unset($nonDefinitionSentences2Info[$sentence]);
            }
        }

        return [
            'definition2sentences' => $definitions2sentences,
            'nonDefinitionSentences' => $nonDefinitionSentences2Info,
        ];
    }

    public function save(Request $request)
    {
        $user = auth()->user();

        $word = WordFactory::findOrCreate(['text' => $request->post('word')], $user);

        $definitionsInfo = (array)$request->post('definitions');
        $definitions = [];

        foreach ($definitionsInfo as $definitionInfo) {
            $definition = DefinitionFactory::findOrCreate([
                'id' => $definitionInfo['id'],
                'text' => $definitionInfo['text'],
            ], $word);

            foreach ($definitionInfo['translations'] as $translationInfo) {
                $translation = TranslationFactory::findOrCreate([
                    'id' => $translationInfo['id'],
                    'text' => $translationInfo['text'],
                ], $definition);
            }

            foreach ($definitionInfo['examples'] as $exampleInfo) {
                $sentence = SentenceFactory::findOrCreate([
                    'id' => $exampleInfo['id'],
                    'text' => $exampleInfo['text'],
                    'translation' => $exampleInfo['translation'] ?? null,
                    'extra' => $exampleInfo['highlights'] ? ['highlights' => $exampleInfo['highlights']] : [],
                ], $definition);
            }

            Translation::query()
                ->where('definition_id', $definition->id)
                ->whereNotIn('id', array_pluck($definitionInfo['translations'], 'id'))
                ->delete();

            Sentence::query()
                ->where('definition_id', $definition->id)
                ->whereNotIn('id', array_pluck($definitionInfo['examples'], 'id'))
                ->delete();

            $definitions[] = $definition;
        }

        return response()->json([
            'word' => $word,
            'definitions' => $definitions,
        ], Response::HTTP_OK);
    }
}
