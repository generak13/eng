import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux'

class AuthManager extends React.Component {

    componentDidUpdate(prevProps) {
        const { dispatch, redirectUrl } = this.props;
        const isLoggingOut = prevProps.isLoggedIn && !this.props.isLoggedIn;
        const isLoggingIn = !prevProps.isLoggedIn && this.props.isLoggedIn;

        if (isLoggingIn) {
            dispatch(push(redirectUrl));
        } else if (isLoggingOut) {
            // do any kind of cleanup or post-logout redirection here
        }
    }

    render() {
        return this.props.children
    }
}

function mapStateToProps(state) {
    return {
        isLoggedIn: state.user.access_token,
        redirectUrl: state.user.redirectUrl,
    }
}

export default connect(mapStateToProps)(AuthManager);