<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimplifyTableRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('sentences_translations');

        Schema::table('sentences', function (Blueprint $table) {
            $table->string('translation')->nullable();
            $table->json('extra')->nullable();
            $table->string('definition_id');

            $table->foreign('definition_id')
                ->references('id')->on('definitions')
                ->onDelete('cascade');
        });

        Schema::table('definitions', function (Blueprint $table) {
            $table->string('word_id');

            $table->foreign('word_id')
                ->references('id')->on('words')
                ->onDelete('cascade');
        });

        Schema::table('translations', function (Blueprint $table) {
            $table->string('definition_id');

            $table->foreign('definition_id')
                ->references('id')->on('definitions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
