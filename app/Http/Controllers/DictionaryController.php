<?php

namespace App\Http\Controllers;

use App\Models\Word;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    public function load(Request $request) {
        $wordsLimit = 15;
        $fromDate = $request->has('from') ? $request->get('from') : null;
        $word = $request->has('word') ? $request->get('word') : null;
        $sortOrder = filter_var($request->get('sortASC', false), FILTER_VALIDATE_BOOLEAN);

        $search = Word
            ::with('definitions', 'definitions.sentences')
            ->limit($wordsLimit);

        if ($fromDate) {
            $search = $search->where('created_at', $sortOrder ? '>=' : '<=', $fromDate);
        }

        if ($word) {
            $search = $search->where('text', 'like', $word.'%');
        }

        $search->orderBy('created_at', $sortOrder ? 'ASC' : 'DESC');
        $data = $search->get();

        return $data;
    }
}
